# Git Basics

Dieses Git Repository soll ein Überblick für die grundlegenden **Git-Befehle** bieten.
Weitere Informationen zu den verwendeten Befehle sowie zu weiteren Git-Befehlen gibt es [[hier](https://git-scm.com/docs)]

## Verwendete Befehle
Eine Tabelle zur Übersicht der verwendeten Befehle

|Befehl | Info |
|:------|:-----|
| `git init` | Erstellung eines neuen Git Repository
| `git add`  | Dokumente/Verzeichnisse zum Staging-Bereich hinzufügen
| `git restore --staged` | Dokumente/Verzeichnisse aus dem Staging-Bereich entfernen
| `git status` | Überblick über die Unterschiede zum letzten Commit
| `git commit` | Anlegen einer neuen Projektversion mit Dokumenten/Verzeichnissen aus dem Staging-Bereich
| `git branch` | Überblick über alle Branches *(`-r` für remote Branches, `-a` für remote und lokale Branches)*
| `git branch <BRANCH> [<BRANCH/COMMIT>]` | Erstellen einer neuen Branch
| `git branch -d` | Löschen einer lokalen Branch
| `git checkout` | Zu einer anderen Branch wechseln
| `git merge` | Zusammenführen von zwei Branches mit einem Merge-Commit
| `git rebase` | Zusammenführen von zwei Branches durch anhängen der Commits
| `git clone` | Remote Repository lokal kopieren
| `git pull` | Lokales Repository mit Änderungen des remote Repository aktualisieren
| `git push` | Remote Repository mit Änderungen des lokalen Repository aktualisieren


## Projekterstellung
Um **Git** zu benutzen muss erst ein **Git Repository** initialisiert (erstellt) werden.
Zur Erstellung eines **lokalen** Git Repositories wird der folgende Befehl verwendet: `git init`

- Falls **kein** Git Repository existiert, wird ein neues Repository **lokal** angelegt.
- Falls bereits ein Git Repository existiert, wird das Repository neu initialisiert, dabei werden keine Daten überschrieben

Zum anlegen von Git Repositories wird standardmäßig das **aktuelle Verzeichnis** genutzt.
Um in einen bestimmten Verzeichnis ein Git Repository anzulegen kann der **Pfad** als weiterer **Parameter** angegeben werden.

**Beispiel:**
Als Beispiel gibt es die folgende Verzeichnisstruktur, wobei das aktuelle Verzeichnis mit einem **\*** markiert ist:

<table>
<tr>
<th> Verzeichnisstruktur </th>
<th> Git Befehl - Erstellung eines Repositories </th>
</tr>

<td>

```
~/DIRECTORY_1
~/DIRECTORY_2 (*)
```

</td>

<td>

- Im Verzeichnis (DIRECTORY_1)
```
git init ~/DIRECTORY_1
```

- Im Verzeichnis (DIRECTORY_2)
```
git init
```

</td>
</table>



## Änderungen speichern
Das **Hinzufügen** von Dateien/Verzeichnissen erfolgt in **zwei Schritten**.
1. Dokumente/Verzeichnisse zum **Staging-Bereich** (staging area) hinzufügen
1. Dokumente/Verzeichnisse des **Staging-Bereichs** als neue Version speichern (**Commit** als neue Projektversion)


### Staging-Bereich
Der Staging-Bereich dient als **Sammlung** von Dokumenten/Verzeichnissen deren **Veränderungen** gespeichert werden sollen.

Dokumente/Verzeichnisse befinden sich **nicht** automatisch im Staging-Bereich.
Es müssen also alle Dokumente/Verzeichnisse **explizit** zum **Staging-Bereich** **hinzugefügt** werden.

Dokumtente/Verzeichnisse können mit `git add <file/directory>` oder `git stage <file/directory>` zum **Staging-Bereich hinzugefügt** werden.
Mit `git add .` werden **alle Dokumente/Verzeichnisse** im **aktuellen Verzeichniss** zum Staging-Bereich hinzugefügt.


Dokumente/Verzeichnisse können mit `git restore --staged <file/directory>` wieder aus dem **Staging-Bereich entfernt** werden.


Um sich eine **Übersicht** zum Stand des Repository (der aktuellen Branch) zu verschaffen kann der Befehl `git status` genutzt werden.
Der Befehl vergleicht dafür den aktuellen Stand mit dem Stand des letzten Commits.

Dabei werden folgende **Informationen berücksichtigt**:
- **Veränderte/bearbeitete** Dokumente/Verzeichnisse
- Dokumente/Verzeichnisse in dem **Staging-Bereich**
- Informationen zur **aktuellen Branch**

**Beispiel:**
- In dem Beispiel wurden bereits Dokumente/Verzeichnisse zum Staging-Bereich hinzugefügt
- Dokumente/Verzeichnisse die sich im **Staging-Bereich befinden** sind mit einem **(+)** markiert

<!--- Staging Table -->
<table>
<tr>
<th> Dokumente/Verzeichnisse </th>
<th> Git Befehl - Staging von Dokumenten/Verzeichnissen </th>
</tr>

<td>

- Status des Repository

```
~/DIRECTORY_1 (+)
~/DIRECTORY_2
~/file_1.txt (+)
~/file_2.txt
```
</td>

<!--- Examples Table -->
<td>
<table>
<tr>
<td>

- Staging DIRECTORY_2

```
git add DIRECTORY_2
```
</td>

<td>

- Staging file_2.txt

```
git add file_2.txt
```
</td>

<td>

- Staging von allen Dokumenten/Verzeichnissen im aktuellen Verzeichniss

```bash
git add .
```
</td>
</tr>

<tr>
<td>

- Resultat:

```
~/DIRECTORY_1 (+)
~/DIRECTORY_2 (+)
~/file_1.txt (+)
~/file_2.txt
```
</td>

<td>

- Resultat:

```
~/DIRECTORY_1 (+)
~/DIRECTORY_2
~/file_1.txt (+)
~/file_2.txt (+)
```
</td>

<td>

- Resultat:

```
~/DIRECTORY_1 (+)
~/DIRECTORY_2 (+)
~/file_1.txt (+)
~/file_2.txt (+)
```
</td>
</tr>
</table>
</td>
</table>


<!--- Unstaging Table -->
<table>
<tr>
<th> Dokumente/Verzeichnisse </th>
<th> Git Befehl - Unstaging von Dokumenten/Verzeichnissen </th>
</tr>

<tr>
<td>

- Status des Repository

```
~/DIRECTORY_1 (+)
~/DIRECTORY_2
~/file_1.txt (+)
~/file_2.txt
```
</td>

<!--- Examples Table -->
<td>
<table>
<td>

- Unstaging DIRECTORY_1

```
git restore --staged DIRECTORY_1
```
</td>

<td>

- Unstaging file_1.txt

```
git restore --staged file_1.txt
```
</td>

<td>

- Unstaging von allen Dokumenten/Verzeichnissen im aktuellen Verzeichniss

```
git restore --staged .
```
</td>
</tr>

<tr>
<td>

- Resultat:

```
~/DIRECTORY_1
~/DIRECTORY_2
~/file_1.txt (+)
~/file_2.txt
```
</td>

<td>

- Resultat:

```
~/DIRECTORY_1 (+)
~/DIRECTORY_2
~/file_1.txt
~/file_2.txt
```
</td>

<td>

- Resultat:

```
~/DIRECTORY_1
~/DIRECTORY_2
~/file_1.txt
~/file_2.txt
```
</td>
</table>
</td>

</tr>
</table>


<!--- Status Table -->
<table>
<tr>
<th> Dokumente/Verzeichnisse </th>
<th> Git Befehl - (Status) Unterschiede zum letzten Commit </th>
</tr>

<tr>
<td>

- Status des Repository

```
~/DIRECTORY_1 (+)
~/DIRECTORY_2
~/file_1.txt (+)
~/file_2.txt
```
</td>

<!--- Examples Table -->
<td>
<table>
<tr>
<td>

- Status der aktuellen Branch

```
git status
```
</td>
</tr>

<tr>
<td>

```
On branch main

Staging Area:
~/DIRECTORY_1
~/file_1.txt

Untracked:
~/DIRECTORY_2
~/file_2.txt
```
</td>
</tr>
</table>
</td>

</tr>
</table>

### Projektversion anlegen
Eine neue **Projektversion** wird mithilfe der Dokumente/Verzeichnisse, die sich im **Staging-Bereich** befinden, erstellt.
Dokumente/Verzeichnisse die sich **nicht** im **Staging-Bereich** befinden werden **nicht** für die neue **Projektversion** verwendet.

Eine neue **Projektversion** wird nur im **lokalen Repository** erstellt!

Eine **Projektversion** wird in **Git**: ***Commit*** genannt und kann mit `git commit -m "commit message"` erstellt werden.
Mit dem **Parameter** `-m` kann ein Text für den **aktuellen Commit** (die neue Projektversion) gespeichert werden.

Dokumente/Verzeichnisse die mit einem ***Commit*** als neue Projektversion gespeichert werden, werden **automatisch** wieder aus dem **Staging-Bereich entfernt**.

**Beispiel:**
- In dem Beispiel wurden bereits Dokumente/Verzeichnisse zum Staging-Bereich hinzugefügt.
- Dokumente/Verzeichnisse die sich im **Staging-Bereich** befinden sind mit einem **(+)** markiert

<table>
<tr>
<th> Dokumente/Verzeichnisse </th>
<th> Git Befehl - Erstellen eines Commits (neue Projektversion erstellen) </th>
</tr>

<td>

```
~/DIRECTORY_1 (+)
~/DIRECTORY_2
~/file_1.txt (+)
~/file_2.txt
```
</td>

<!--- Commit Table -->
<td>
<table>
<tr>
<td>

- **Commit** alle Dokumente/Verzeichnisse im Staging-Bereich
```
git commit -m "Add files/directories"
```
</td>
</tr>

<tr>
<td>

- Resultat:
```
~/DIRECTORY_1
~/DIRECTORY_2
~/file_1.txt
~/file_2.txt
```
</td>
</tr>
</table>
</td>
</table>



## Branching

In Git gibt es **Branches**. Eine Branch ist ein **Verweis** (Pointer) auf einen bestimmten **Commit**.

<table>
<tr>
<th> Commits und Branches </th>
<th> Info </th>
</tr>

<tr>
<td>

```
 b1              main
 V                V
c1 <- c2 <- c3 <-c4
```
</td>


<td>

- **Commits** (c1, c2, c3 und c4)
    - c4 ist abhängig von c3
    - c3 ist abhängig von c2
    - c2 ist abhängig von c1
- **Branches** (b1, main)
    - Verweisen auf unterschiedliche Commits

</td>
</tr>
</table>

Änderungen die auf einer Branch mit einem Commit gespeichert werden, sind **nur** auf dieser Branch (als Commit) vorhanden.
Jede Branch kann eigene Commits mit unterschiedliche Dokumente/Verzeichnisse speichern.
Diese Unterschiede werden als Verzweigungen der Commits sichtbar. (`git log --graph --oneline --all`)

<table>
<tr>
<th> Commits und Branches </th>
<th> Info </th>
</tr>

<tr>
<td>

```
               b2
               V
 b1     c3 <- c4        main
 V      /               V
c1 <- c2 <- c5 <-c6 <- c7
```

</td>


<td>

- **Commits** (c1, c2, c3, c4, c5, c6 und c7)
    - c4 ist der einzige Commit abhängig von c3
    - c3 und c4 sind abhängig von c2
    - c5, c6 und c7 sind abhängig von c2
- **Branches** (b1, b2 und main)
    - Verweisen auf unterschiedliche Commits

</td>
</tr>
</table>

Häufig gibt es eine **Hauptbranch** (main/master) von der andere Branches abzweigen.


### Branch Operationen
Um Operationen mit Branches durchzuführen ist es sinnvoll einen Überblick zu allen existierenden Branches zu bekommen.

Die **lokalen** Branches können mit dem Befehl `git branch` angezeigt werden.\
Mit den Optionen `-r` werden die **remote** Branches und mit `-a` **lokal** und **remote** Branches angezeigt.

Zum anzeigen der Branches kann zusätzlich die `--list` Option gesetzt werden, um mit einem Muster zu filtern.
Die aktuelle lokale Branch wird mit einem **\*** markiert.


Zum **erstellen** einer neuen Branch kann `git branch <BRANCH_NAME> [<COMMIT/BRANCH>]` verwendet werden.\
Mit dem **Parameter** (`[<COMMIT/BRANCH>]`) kann ein **Commit** oder eine **Branch** als **Grundlage** für die zu erstellende Branch angegeben werden.
Ohne diesen zusätzlichen Parameter ist die Grundlage der **letzte Commit** der **aktuellen Branch**.\
*Mit `git checkout -b <NEUE_BRANCH> [<COMMIT/BRANCH>]` wird zusätzlich zu der neu erstellten Branch gewechselt.*

Zum **entfernen** einer Branch kann `git branch -d <BRANCH_NAME>` verwendet werden.

Zum **wechseln** zu einer anderen Branch kann `git checkout <BRANCH_NAME>` verwendet werden.


___

*Anstatt eine Branch anzugeben kann auch ein Commit durch den Commit-Hash oder einen relativen Pfad angegeben werden!\
Ein relativer Pfad verweist auf einen Commit zuvor, von dem der aktuelle Commit abhängt.
Ein relativer Pfad kann von einem Commit (`Commit-Hash~1`), einer Branch (`Branch~1`) oder dem HEAD (`HEAD~1`) angegeben werden.*


**Beispiel:**
- Commits werden mit c markiert
- Aktuelle Branch wird mit einem (**\***) markiert


<!--- Branch Creation Table -->
<table>
<tr>
<th> Commits und Branches </th>
<th> Git Befehl - Erstellung einer Branch </th>
</tr>

<tr>
<td>

```
b1                     main (*)
V                       V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>


<!--- Examples Table -->
<td>
<table>
<tr>
<td>

- Erstellen einer Branch mit der aktuellen Branch
```
git branch b2
```
</td>

<td>

- Erstellen einer Branch mit einer anderen Branch (*b1*)
```
git branch b2 b1
```
</td>

<td>

- Erstellen einer Branch mit einem Commit-Hash (*c3*)
```
git branch b2 c3
```
</td>
</tr>

<tr>
<td>

- Resultat:
```
                       b2
b1                     main (*)
V                       V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>
<td>

- Resultat:
```
b2
b1                     main (*)
V                       V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>

<td>

- Resultat:
```
b1          b2         main (*)
V           V           V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>
</tr>
</table>

</td>
</tr>
</table>


<!--- Branch deletion Table -->
<table>
<tr>
<th> Commits und Branches </th>
<th> Git Befehl - Löschen einer Branch </th>
</tr>

<td>

```
b1                     main (*)
V                       V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>


<td>

<!--- Example Table -->
<table>
<tr>
<td>

- Löschen einer anderen Branch (*b1*)
```
git branch -d b1
```
</td>
</tr>

<tr>
<td>

- Resultat:
```
                       main (*)
                        V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>
</tr>
</table>
</td>
</table>



<!--- Branch switching Table -->
<table>
<tr>
<th> Commits und Branches </th>
<th> Git Befehl - Wechseln zu anderen Commits/Branches </th>
</tr>

<td>

```
b1                     main (*)
V                       V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>

<td>
<!--- Example Table -->
<table>
<tr>
<td>

- Wechseln zu einer anderen Branch (*b1*)
```
git checkout b1
```
</td>

<td>

- Wechseln zu einem Commit-Hash (*c3*)
```
git checkout c3
```
</td>
</tr>


<tr>
<td>

- Resultat:
```
b1 (*)                 main
V                       V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>

<td>

- Resultat:
```
b1         (*)         main
V           V           V
c1 <- c2 <- c3 <- c4 <- c5
```
</td>
</tr>
</table>
</td>

</table>


### Merging
In einem **Git Repository** kann es verschiedene Branches mit unterschiedlichen Dokumenten/Verzeichnissen geben.
Um einen eindeutigen Projektstand wiederherzustellen, können Branches zusammengefügt werden.
Häufig werden Branches wieder mit einer **Hauptbranch** zusammengefügt, die dann wieder als Ausgangspunkt für neue Branches dient.

Das Zusammenfügen von zwei Branches kann mit `git merge` oder `git rebase` erfolgen.

**Merge:**\
Mit `git merge` entsteht ein neuer Commit auf der aktuellen Branch.
Der Merge-Commit beinhaltet beide Branches als Vorgänger.

**Rebase:**\
Mit `git rebase` werden die Commits einer Branch ans ende der aktuellen Branch "geschoben".
Dabei wird der Abzweig mit den Commits entfernt.


<table>
<tr>
<th> Commits und Branches </th>
<th> Git Befehl - Mergen einer Branch </th>
</tr>

<tr>
<td>

```
        c3 <- c4                    b1
       /
c1 <- c2 <- c5 <-c6                 main
```
</td>

<td>
<!--- Merging Table -->
<table>
<tr>
<td>

- Mergen einer Branch (*b1*) in die aktuelle Branch (*main*)
```
git checkout main
git merge b1
```
</td>

<td>

- Mergen einer Branch (*main*) in die aktuelle Branch (*b1*)
```
git checkout b1
git merge main
```
</td>
</tr>


<tr>
<td>

- Resultat:
```
        c3 <- c4 <-----              b1
       /               \
c1 <- c2 <- c5 <-c6 <- c7            main (*)
```
</td>

<td>

- Resultat:
```
        c3 <- c4 <- c7      b1 (*)
       /           /
c1 <- c2 <- c5 <-c6         main
```
</td>
</tr>
</table>
</td>
</tr>
</table>


<table>
<tr>
<th> Commits und Branches </th>
<th> Git Befehl - Mergen einer Branch </th>
</tr>

<tr>
<td>

```
        c3 <- c4                    b1
       /
c1 <- c2 <- c5 <-c6                 main
```
</td>

<td>
<table>
<tr>
<td>

- Rebase Commits von der aktuellen Branch (*main*) auf die angegebene Branch (*b1*)
```
git checkout main
git rebase b1
```
</td>

<td>

- Rebase Commits von der aktuellen Branch (*b1*) auf die angegebene Branch (*main*)
```
git checkout b1
git rebase main
```
</td>
</tr>


<tr>
<td>

- Result:
```
              b1
              V
        c3 <- c4 <- c5 <- c6        main (*)
       /
c1 <- c2
```
</td>

<td>

- Result:
```
                main
                 V
c1 <- c2 <- c5 <-c6 <- c3 <- c4         b1 (*)
```
</td>
</tr>

</table>
</td>
</tr>
</table>



## Remote Repositories
Git Repository können auch auf **Servern** (Online) gespeichert werden.\
Da das Repository Online gespeichert wird, kann **unabhängig** vom benutztem **Gerät** auf dieses remote Repository zugegriffen werden.
Dabei können mehrere Entwickler an einem Projekt (Repository) **zusammenarbeiten**.

Es gibt verschiedene **Sichtbarkeitsstufen** die für remote Repository festgelegt werden können:
- **Private** (Benutzer müssen zum Projekt (Repository) hinzugefügt werden - einzelned/als Gruppe)
- **Internal** (Benutzer der gleichen Organisation)
- **Public** (Alle Benutzer - ohne Authentifizierung)



### Zusammenhang von remote und lokalen Repositories
Um Projekte Online unabhängig vom aktuellen Gerät zu speichern oder mit anderen Entwicklern an einem Projekt zu arbeiten, ist die Erstellung eines **remote Repository** sinnvoll.
Die verschiedenen Plattformen auf denen Git Projekte gespeichert werden dienen dabei vorallem als globaler Speicher/Zugriffsort.\
Änderungen werden meistens in Entwicklungsumgebungen auf dem lokalen Gerät erstellt, indem das Projekt lokal kopiert und im Anschluss das remote Repository aktualisiert wird.
> Änderungen des remote Repository können auch über die Web-Anwendung erstellt werden

**Schritte zum lokalen Bearbeiten eines remote Repository:**
1. **Kopieren oder Aktualisieren eines remote Repository**
    > Falls das remote Repository bereits als **lokales Repositoy existiert**, ist es ausreichend das Repository zu **aktualisieren**.
    > Ansonsten muss das remote Repository **kopiert** werden.

    - **Kopieren des remote Repository**\
        Durch das Kopieren entsteht ein **lokales Repository** mit dem aktuellen Stand des remote Repository
        > Es gibt zwei Repository von dem Projekt:
        > - Ein **remote Repository** (Online)
        > - Ein **lokales Repository** (auf dem lokalen Gerät)

        Mit dem folgenden Befehl kann ein remote Repositoy kopiert werden:
        ```
        git clone <URL des remote Repository>
        ```

    - **Aktualisieren des lokalen Repository**\
        **Änderungen** vom remote Repository müssen explizit zum lokalen Repository hinzugefügt werden

        Mit dem folgendem Befehl kann ein lokales Repository aktualisiert werden:
        ```
        git pull
        ```

2. **Bearbeiten des Projekts**\
    **Änderungen** des Projekts können am lokalen Repository durch `Commits` vorgenommen werden
    > Die lokalen `Commits` sind nur im lokalen Repository verfügbar und müssen explizit zum remote Repository hinzugefügt werden!

3. **Remote Repository aktualisieren**\
    Durch das aktualisieren des remote Repository mit dem lokalen Repository werden die lokalen `Commits` zum remote Repository hinzugefügt

    Mit dem folgenden Befehl können Änderungen vom lokalen zum remote Repository hinzugefügt werden:
    ```
    git push origin <Branch des remote Repository>
    ```

**Beispiel:**
- Commits werden mit c markiert

<table>
<tr>
<th> Git Befehle - Bearbeiten eines Repository </th>
<th> Remote Repository (origin main) </th>
<th> Lokales Repository (main) </th>

</tr>

<tr>
<td>
</td>


<td>

```
c1 <- c2
```
</td>

<td>
</td>
</tr>

<tr>
<td>

- Kopieren des remote Repository
```
git clone <URL des remote Repository>
```
</td>


<td>

```
c1 <- c2
```
</td>


<td>

```
c1 <- c2
```
</td>
</tr>

<tr>
<td>

- Änderungen zum lokalen Repository hinzufügen (*c3*)
```
git commit
```
</td>


<td>

```
c1 <- c2
```
</td>


<td>

```
c1 <- c2 <- c3
```
</td>
</tr>

<tr>
<td>

- Änderungen zum remote Repository hinzufügen
```
git push origin main
```
</td>


<td>

```
c1 <- c2 <- c3
```
</td>


<td>

```
c1 <- c2 <- c3
```
</td>
</tr>

<tr>
<td>

- Änderungen am remote Repository
> Durch andere Entwickler oder von einem anderem Gerät
</td>


<td>

```
c1 <- c2 <- c3 <- c4
```
</td>


<td>

```
c1 <- c2 <- c3
```
</td>
</tr>

<tr>
<td>

- Lokales Repository aktualisieren
```
git pull origin main
```
</td>


<td>

```
c1 <- c2 <- c3 <- c4
```
</td>


<td>

```
c1 <- c2 <- c3 <- c4
```
</td>
</tr>
</table>






### Nutzen eines remote Repository
Falls ein Projekt als remote Repository **gespeichert** oder **öffentlich** zugänglich gemacht werden soll können folgende **Möglichkeiten** auftreten:
- Ein [**lokales Verzeichnis**](#lokales-projekt) (Projekt) soll in einem remote Repository gespeichert werden
- Ein [**lokales Git Repository**](#lokales-repository) soll in einem remote Repository gespeichert werden

Für die Nutzung eines remote Repository muss dieses Repository erst erstellt werden.

> Ein remote Repository hat eigene Branches (mit `origin` bezeichnet).\
> Diese Upstream Branches können mit lokale Branches verknüpft werden.\
> Dabei haben lokale Branches maximal ein Upstream Branch zu welcher lokale Commits für das remote Repository gepusht werden.

### Erstellen eines remote Repository
Es gibt unterschiedliche **Plattformen** die das Hosten von Git Repository anbieten:
- GitHub
- GitLab
- Bitbucket


#### Erstellung eines remote Repository mit GitLab
- `New Project` (rechts oben)
- `Create blank project` (oder von einem Template: `Create from template`)
- Repository Einstellungen
    - Projektnamen
    - Sichtbarkeitslevel (Private, Internal oder Public)
    - README Dokument
- `Create project`


### Lokales Projekt
In diesem Fall **existiert** bereits ein Projekt **ohne** Git Repository. Dieses Projekt soll jetzt mit einem **remote** Repository gespeichert werden.

**Folgende Schritte sind notwendig:**

0. Erstellen eines remote Repository
    > Auf einer Plattform die Git Repository hosted - Speicherort des Repository
1. Navigation in das Verzeichnis des lokalen Projekts
    ```
    cd [lokales Projekt]
    ```
2. Erstellen eines lokalen Git Repository
    ```
    git init --initial-branch=main
    ```
    > Bei GitLab standardmäßig die Hauptbranch: main
3. Verknüpfung zum erstellten remote Repository herstellen
    ```
    git remote add origin [URL des erstellten remote Repository]
    ```
4. Alle Dokumente/Verzeichnisse im Repository speichern
    ```
    git add .
    git commit -m "Initial commit"
    ```
5. Die lokale Branch mit der Upstream Branch verknüpfen und den lokalen Commit zum remote Repository hinzufügen
    ```
    git push --set-upstream origin main
    ```
    > Verknüpfung der lokalen Branch `main` mit der Upstream Branch (remote Branch) `main`\
    > Dadurch werden Commits automatisch von der lokalen Branch `main` auf die remote Branch `main` gepusht


### Lokales Repository
In diesem Fall gibt es bereits ein **lokales** Git Repository auf dem Gerät, das jetzt mit dem zuvor erstellten **remote** Repository **verknüpft** werden soll.

**Folgende Schritte sind notwendig:**

0. Erstellen eines remote Repository
    > Auf einer Plattform die Git Repository hosted - Speicherort des Repository
1. Navigation in das Verzeichnis des lokalen Projekts
    ```
    cd [lokales Projekt]
    ```
2. Entferen der Verknüpfung zu einem möglichen remote Repository
    ```
    git remote rename origin old-origin
    ```
3. Verknüpfung zum erstellten remote Repository herstellen
    ```
    git remote add origin [URL des erstellten remote Repository]
    ```
4. Festlegen der Upstream branches
    ```
    git push --set-upstream origin --all
    ```
    > Verknüpfung der lokalen Branch `main` mit der Upstream Branch (remote Branch) `main`\
    > Dadurch werden Commits automatisch von der lokalen Branch `main` auf die remote Branch `main` gepusht
5. Festlegen der Upstream tags
    ```
    git push --set-upstream origin --tags
    ```



## SSH Authentifizierung
Anstatt für jeden Zugriff auf das remote Repository immer den jeweiligen **Benutzer** und das zugehörige **Password** einzugeben, kann ein **SSH Key** verwendet werden.\
Dazu wird auf dem **lokalen Gerät** ein **SSH Key erstellt**. (Es werden zwei Keys erstellt: ein **öffentlicher** (public) und ein **privater** (private) Schlüssel)\
Der **öffentliche** Schlüssel wird auf der **Plattform** für die remote Repository **hinterlegt**.


**Folgende Schritte sind notwendig:**
1. Erstellen eines SSH Keys
    ```
    ssh-keygen -t ed25519 -C "<E-Mail Adresse>" -f ~/.ssh/id_example
    ```
    > Es wird ein **öffentlicher** (public - `.pub`) und ein **privater** (private) Schlüssel erstellt.\
    > **Optionen:**
    > - `-t` zum angeben des verwendeten **SSH Typen** - [SSH Typen für GitLab](https://docs.gitlab.com/ee/user/ssh.html#supported-ssh-key-types)
    > - `-C` zum hinzufügen eines **Kommentars** (zum Beispiel die genutzte E-Mail Adresse auf der Plattform)
    > - `-f` zum festlegen des **Speicherortes** und **Names** des Keys (hier `id_example`)
2. **Privaten** (private) SSH Key zum ssh-agent hinzufügen
    - SSH-Agent starten
        ```
        eval $(ssh-agent -s)
        ```
    - **Privaten** (private) SSH Key hinzufügen
        ```
        ssh-add ~/.ssh/id_example
        ```
3. SSH-Client konfigurieren (bei mehreren SSH-Keys)
    - Bearbeiten des `~/.ssh/config` Dokuments
        ```
        # Beispiel Konfiguration für GitLab
        HOST gitlab.com
            PreferredAuthentications publickey
            IdentityFile ~/.ssh/id_example
        ```
4. **Öffentlichen** (public) SSH Key zur Plattform hinzufügen\
    **Schritte für GitLab:**
    - `Edit profile`
    - `SSH Keys`
    - `Add new key`
    - Einfügen des **öffentlichen** (public) Key mit SSH Typen\
        Beispiel: `ssh-ed25519 AA..`
    - `Add key`
5. Repository mit SSH clonen\
    Um den SSH Key zu verwenden, muss das Repository über SSH geclont werden.\
    Die aktuelle remote URL kann mit dem folgenden Befehl angezeigt werden:
    ```
    git remote -v
    ```





